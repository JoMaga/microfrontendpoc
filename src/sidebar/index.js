import React from 'react'
import ReactDOM from 'react-dom'
import singleSpaReact from 'single-spa-react'
import Sidebar from './sidebar.component.js'

const domElementGetter = () => {
  let el = document.getElementById('single-spa-application:navigation')
  if (!el) {
    el = document.createElement('div')
    el.id = 'sidebar'
    document.body.appendChild(el)
  }

  return el
}

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Sidebar,
  domElementGetter,
})

export const bootstrap = props => reactLifecycles.bootstrap(props)
export const mount = props => reactLifecycles.mount(props)
export const unmount = props => reactLifecycles.unmount(props)
