import React from 'react'
import e from '../event-bus'
// import './sidebar.component.scss'

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentPage: 'general'
    }

    this.navigationHandler = this.navigationHandler.bind(this)
    this.sendNavRequest = this.sendNavRequest.bind(this)

  }

  componentDidMount() {
    e.on('received', this.navigationHandler)
  }

  componentDidUnmount() {
    e.off('received', this.navigationHandler)
  }

  navigationHandler(page) {
    this.setState({
      currentPage: page
    })
  }

  sendNavRequest(page) {
    console.log(page)
    e.emit('navigation', page)
  }

  render() {
    return (
      <div className="sidebar-wrapper">
        <div className="options" onClick={()=>this.sendNavRequest('general')}>General</div>
        <div className="options" onClick={()=>this.sendNavRequest('configuration')}>Configuration</div>
      </div>
    )
  }
}
