import { APP_BASE_HREF } from '@angular/common';
import { enableProdMode, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import AngularApp from './index.component.ts';
import ConfigurationComponent from './pages/configuration/configuration.component.ts';

enableProdMode()

@NgModule({
  imports: [
    BrowserModule,
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/angular/' }],
  declarations: [
    AngularApp,
    ConfigurationComponent
  ],
  bootstrap: [AngularApp]
})
export default class MainModule {
}
