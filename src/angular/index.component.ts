import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import e from '../event-bus';

@Component({
  selector: 'AngularApp',
  template: `
		<div style="margin-top: 100px;">
    <ng-container *ngIf="!showConfigPage">
    <h1>This was written in Angular</h1>
    <p>{{message}}</p>
    </ng-container>
      <configuration-page *ngIf="showConfigPage"></configuration-page>
		</div>
	`,
})
export default class AngularApp {
  public showConfigPage: boolean = false;
  message: string = "Message from React should appear here 😱"

  constructor(@Inject(ChangeDetectorRef) private changeDetector: ChangeDetectorRef) { }

  ngAfterContentInit() {
    e.on('message', message => {
      this.message = message.text
      this.changeDetector.detectChanges()
      this.returnMessageToReactWhenReceived()
    })
    e.on('navigation', page => {
      if (page === "configuration") {
        console.log("triggered", page)
        this.showConfigPage = true;
        this.changeDetector.detectChanges()
      }
      else {
        this.showConfigPage = false;
        this.changeDetector.detectChanges()
      }
    })
  }

  returnMessageToReactWhenReceived() {
    e.emit('received', { text: 'Woohoo! Hello from Angular! 🎉' })
  }
}
