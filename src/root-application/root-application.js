import { start, registerApplication } from 'single-spa'

//Url Prefix
const hashPrefix = prefix => location => location.hash.startsWith(`#${prefix}`)
//React Apps
registerApplication('react-content', () => import('../react/index.js'), hashPrefix('/'))
registerApplication('sidebar', () => import('../sidebar/index.js'), hashPrefix('/'))
//Angular Apps
registerApplication('angular-content', () => import('../angular/index.js'), hashPrefix('/'))

start()
